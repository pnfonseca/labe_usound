/*
 * test_usons.c
 * 
 * Copyright 2016 Pedro Fonseca <pf@lua.pt>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#define MAX_DELAY  40*2000
#define MIN_READING 800

#include "labE.h"
#include "labE_usound.h"



int
main (void)
{
  /* Variable declarations go here */
  unsigned long delay, reading;
  unsigned int waiting_echo;
  float dist; 
  
  /* initPIC32() makes all the required initializations to the
   * PIC32 hardware platform */
  initPIC32 ();

  uSound_init();
    
  uSound_control(0);
    
  while (TRUE)
    {
      delay_ms(200);
      /* Sends a ultrasound burst, 600us long */
      uSound_control(1);
      delay_us(600);
      uSound_control(0);
      
      /*
       * Cycles waiting for the echo. The cycle ends when reading
       * is above MIN_READING or when MAX_DELAY has ellapsed.
       */
      waiting_echo = 1;
      while(waiting_echo)
      {
        reading = analog(Analog6);
        
        /* Note: CoreTimer is reset in the call to delay_us after
         * turning ultrasounds on. */
        delay = readCoreTimer();
        if(reading > MIN_READING){
          waiting_echo = 0;
        }
        if(delay > MAX_DELAY){
          waiting_echo = 0;
        }
      }
  
      /* If reading the echo was successful, print distance... */
      if(reading > MIN_READING){
        /* Convert delay to distance in mm */
        dist = (delay / 40)*343/1000;
        printInt10((int)dist);
      }
      else{
        /* ...otherwise, signal no reading. */
        printStr("----   ");
      }
      printStr("\r");
    }
  return (0);
}
