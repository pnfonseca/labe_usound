/*
 * test_usons2.c
 * 
 * Copyright 2016 Pedro Fonseca <pf@lua.pt>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#define MIN_READING 512

#include "labE.h"
#include "labE_usound.h"



int
main (void)
{
  /* Variable declarations go here */
  unsigned long delay, delay1;
  unsigned int reading; 
  unsigned int waiting_echo;
  unsigned long dist; 
  
  /* initPIC32() makes all the required initializations to the
   * PIC32 hardware platform */
  initPIC32 ();

  /* uSound_init() initializes all hardware to generate the 
   * ultrassound burst and measure the delay.
   */
  uSound_init();
    
  while (TRUE)
    {
      delay_ms(200);
      
      /* Sends a ultrasound burst, 400us long */
      uSound_burst(400/25);
     
      /*
       * Cycles waiting for the echo. The cycle ends when reading
       * is above MIN_READING or when MAX_USOUND_DELAY has ellapsed.
       */
      waiting_echo = 1;

      while(waiting_echo)
      {
        reading = analog(Analog6);
        
        delay = uSound_get_delay();
        
        if(reading > MIN_READING){
          waiting_echo = 0;
        }
        
        if(delay > MAX_USOUND_DELAY){
          waiting_echo = 0;
        }
      }

      /* If reading the echo was successful, print distance... */
      if(reading > MIN_READING){
        /* Convert delay to distance in mm */
        dist = (delay*330)/80;
        printInt10(dist);
        printStr("   ");
      }
      else{
        /* ...otherwise, signal no reading. */
        printStr("----   ");
      }
      printStr("\r");

    }
  return (0);
}
